# Игра Пятнашки 
## Системные требования  
Windows 10 x64  
Монитор
Клавиатура

## Для установки
Откройте файл PyatnashkiSetup.exe

## Для пользования без установки
Откройте файл Pyatnashki.exe по пути setup\Release\Pyatnashki.exe

## Для разработки
Использовать CMake 3.22 и Visual Studio 2019

